﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Laboratorio_REST_API_Comercio.Logica
{
    // Clase objeto para manejar los cantones.
    public class Canton
    {

        public Canton(int cod_canton, string nombre, int cod_provincia)
        {
            this.cod_canton = cod_canton;
            this.nombre = nombre;
            this.cod_provincia = cod_provincia;
        }

        public Canton() { }

        public int cod_canton { get; set; }
        public string nombre { get; set; }
        public int cod_provincia { get; set; }
    }
}