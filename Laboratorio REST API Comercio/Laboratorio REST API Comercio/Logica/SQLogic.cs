﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text.Json;
using System.Windows.Forms;

namespace Laboratorio_REST_API_Comercio.Logica
{
    public class SQLogic : SQL
    {
        public List<Canton> traer_cantones()
        {
            string sql_query = "SELECT * FROM CANTON";
            DataSet ds = Fetch(sql_query);

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<Canton> lista_cantones = new List<Canton>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    int cod_canton = Convert.ToInt32(ds.Tables[0].Rows[i]["Cod_Canton"]);
                    string nombre = ds.Tables[0].Rows[i]["Nombre"].ToString();
                    int cod_provincia = Convert.ToInt32(ds.Tables[0].Rows[i]["Cod_Provincia"]);
                    lista_cantones.Add(new Canton(cod_canton, nombre, cod_provincia));
                }
                //MessageBox.Show(JsonSerializer.Serialize(lista_cantones));
                return lista_cantones;
            }
            else
            {
                return null;
            }
        }

    }
}