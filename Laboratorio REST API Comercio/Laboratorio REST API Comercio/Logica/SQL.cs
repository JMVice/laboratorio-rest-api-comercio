﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Laboratorio_REST_API_Comercio.Logica
{
    public class SQL
    {
        //string conn = ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        string conn = "Data Source=10.172.61.116;Initial Catalog=Comercio;User ID=admin;Password=admin123";

        public void Query(string query)
        {
            SqlConnection connection = new SqlConnection(this.conn);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;
            cmd.Connection = connection;

            connection.Open();
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public DataSet Fetch(string query)
        {
            SqlConnection connection = new SqlConnection(this.conn);
            SqlDataAdapter dadapter = new SqlDataAdapter(query, connection);
            DataSet ds = new DataSet();
            connection.Open();
            dadapter.Fill(ds);
            connection.Close();
            return ds;
        }
    }
}