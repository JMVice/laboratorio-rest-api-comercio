﻿using System.Web;
using System.Web.Mvc;

namespace Laboratorio_REST_API_Comercio
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
