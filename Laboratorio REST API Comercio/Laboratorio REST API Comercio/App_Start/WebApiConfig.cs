﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Laboratorio_REST_API_Comercio
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //////////////////////////////////////////////////////////////////////
            // Api que responde a api/canton
            config.Routes.MapHttpRoute(
                name: "CantonApi",
                routeTemplate: "api/{controller}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
