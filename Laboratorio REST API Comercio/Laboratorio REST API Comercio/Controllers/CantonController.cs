﻿using Laboratorio_REST_API_Comercio.Logica;
using System.Collections.Generic;
using System.Web.Http;
// Usamos el using de System.Text.Json para hacer uso de sus metodos.
// Inicialmente no está instalado. Debemos instalarlo en el administrador
// de paquetes NuGet buscando e intalando System.Text.Json .
using System.Text.Json;

namespace Laboratorio_REST_API_Comercio.Controllers
{
    public class CantonController : ApiController
    {
        // GET api/<controller>
        public string Get()
        {
            // Declaracion de variable SQLogic para uso de sus metodos.
            SQLogic sqlogic = new SQLogic();

            // Traemos la lista de cantones guardada en la base de datos
            // y la guardamos en la variable lista_cantones.
            List<Canton> lista_cantones = sqlogic.traer_cantones();

            // Retornamos un string como resultado de la API formateado
            // como Json con la clase JsonSerializer y metodo Serialize.
            return JsonSerializer.Serialize(lista_cantones);
        }
    }
}